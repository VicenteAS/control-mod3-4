'use strict';

const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Reean',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Rishi',
    'Arun',
    'Kenton',
];

const result = names.reduce((acc, name) => {
    if (!acc.includes(name)) {
        acc.push(name);
    }
    return acc;
}, []);
console.log(result);
