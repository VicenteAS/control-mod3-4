'use strict';
const h1 = document.querySelector('h1');

setInterval(() => {
    const now = new Date();
    let hour = now.getHours();
    let minutes = now.getMinutes();
    let seconds = now.getSeconds();

    const date = now.toLocaleDateString('es-ES', {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
    });
    h1.innerHTML = `${hour} : ${minutes} : ${seconds} , ${date}`;
    console.log(`${hour} : ${minutes} : ${seconds} , ${date}`);
}, 5000);
